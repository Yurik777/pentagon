<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;

/**
 * Interface SubjectInterface
 *
 * @package App\Services
 */
interface SubjectInterface
{
    /**
     * @return Collection
     */
    public function getData();

    /**
     * @param array $data
     *
     * @return JsonResponse
     */
    public function saveData($data);

    /**
     * @param array $data
     *
     * @return array
     */
    public function normaliseData($data);
}
