<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Class ProductService
 *
 * @package App\Services
 */
class ProductService implements SubjectInterface
{

    /**
     * @inheritDoc
     */
    public function getData()
    {
        return Product::all();
    }

    /**
     * @inheritDoc
     *
     * @throws ValidationException
     */
    public function saveData($data)
    {
        $product = $this->normaliseData($data);

        $rules = [
            'title' => 'required',
            'sku' => 'required|unique:products',
            'image' => 'required'
        ];

        $validator = Validator::make($product, $rules);

        if ($validator->fails()) {
            return new JsonResponse([
                'error' => 'Product not saved. A product with this SKU already exists. Try again.'
            ]);
        }

        Product::query()->create($validator->validate());

        return new JsonResponse([
            'success' => 'Product received and saved successfully',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function normaliseData($data) {
        $result = [];

        foreach ($data as $key => $value) {

            if (strstr($key, '\\', true) === 'image') {
                $typeImage = 'image/' . explode(';', $key)[1] . ';base64';
                $bodyImage = rtrim(ltrim($value, $key . '{'), '}');
                $result['image'] = str_replace(
                    '\\',
                    '',
                    rtrim($typeImage . ',' . $bodyImage, '}"')
                );
                continue;
            }

            if ($key === 'SKU') {
                $result['sku'] = $value;
                continue;
            }

            $result[$key] = $value;
        }

        return $result;
    }
}
