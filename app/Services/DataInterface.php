<?php

namespace App\Services;

use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;

/**
 * Interface DataInterface
 *
 * @package App\Services
 */
interface DataInterface
{
    /**
     * @param InitService $initService
     *
     * @return PromiseInterface|Response|mixed
     */
    public function getData(InitService $initService);

    /**
     * @param string $data
     *
     * @return string
     */
    public function typeData($data);

    /**
     * @param string $data
     *
     * @return array
     */
    public function parseData($data);
}
