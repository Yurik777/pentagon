<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

/**
 * Class InitService
 *
 * @package App\Services
 */
class InitService implements InitInterface
{

    /**
     * @inheritDoc
     */
    public function init()
    {
        $response = json_decode(Http::withBasicAuth(
            config('pentagon.auth.client_id'),
            config('pentagon.auth.client_secret')
        )->post(config('pentagon.api.domain') . config('pentagon.api.init')), true);

        $this->saveToken($response['access_token']);
    }

    /**
     * @inheritDoc
     */
    public function saveToken($token)
    {
        session(['token' => $token]);
    }

    /**
     * @inheritDoc
     */
    public function getToken()
    {
        return session('token');
    }
}
