<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

/**
 * Class DataService
 *
 * @package App\Services
 */
class DataService implements DataInterface
{
    /**
     * @inheritDoc
     */
    public function getData(InitService $initService)
    {
        return Http::withToken($initService->getToken())
            ->get(config('pentagon.api.domain') . config('pentagon.api.get_random_test_feed'));
    }

    /**
     * @param string $data
     *
     * @return false|string
     */
    public function typeData($data)
    {
        return trim(strstr($data, ':', true), '"');
    }

    /**
     * @param string $data
     *
     * @return array
     */
    public function parseData($data)
    {
        $result = [];

        $dataString = ltrim(strstr($data, ':'), ':');
        $dataArray = explode('||', $dataString);

        foreach ($dataArray as $item) {
            $key = strstr($item, '{', true);
            $result[$key] = rtrim(ltrim($item, $key . '{'), '}');
        }

        return $result;
    }
}
