<?php

namespace App\Services;

use App\Models\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Class OrderService
 *
 * @package App\Services
 */
class OrderService implements SubjectInterface
{

    /**
     * @inheritDoc
     */
    public function getData()
    {
        return Order::all();
    }

    /**
     * @inheritDoc
     *
     * @throws ValidationException
     */
    public function saveData($data)
    {
        $order = $this->normaliseData($data);

        $rules = [
            'id' => 'required|unique:orders',
            'total' => 'required',
            'shipping_total' => 'required',
            'create_time' => 'required',
            'timezone' => 'required'
        ];

        $validator = Validator::make($order, $rules);

        if ($validator->fails()) {
            return new JsonResponse([
                'error' => 'Order not saved. A order with this ID already exists. Try again.'
            ]);
        }

        Order::query()->create($validator->validate());

        return new JsonResponse([
            'success' => 'Order successfully received and saved',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function normaliseData($data) {
        $result = [];

        foreach ($data as $key => $value) {
            if ($key === 'timezone') {
                $result[$key] = str_replace('\\', '', rtrim($value, '}"'));
            }

            if ($key === 'total' || $key === 'shipping_total') {
                $result[$key] = (float)$value;
            }

            if ($key === 'id' || $key === 'create_time') {
                $result[$key] = (int)$value;
            }
        }

        return $result;
    }
}
