<?php

namespace App\Services;

/**
 * Interface InitInterface
 *
 * @package App\Services
 */
interface InitInterface
{
    /**
     * @return mixed
     */
    public function init();

    /**
     * @param string $token
     */
    public function saveToken($token);

    /**
     * @return string
     */
    public function getToken();
}
