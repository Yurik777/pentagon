<?php

namespace App\Http\Controllers;

use App\Services\DataService;
use App\Services\InitService;
use App\Services\OrderService;
use App\Services\ProductService;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\View\View;

/**
 * Class Controller
 *
 * @package App\Http\Controllers
 */
class DataController extends BaseController
{
    const ORDER = 'order';
    const PRODUCT = 'product';

    /** @var OrderService */
    protected $orderService;

    /** @var ProductService */
    protected $productService;

    /**
     * Controller constructor.
     *
     * @param OrderService   $orderService
     * @param ProductService $productService
     */
    public function __construct(
        OrderService $orderService,
        ProductService $productService
    ) {
        $this->orderService = $orderService;
        $this->productService = $productService;
    }

    /**
     * @return View
     */
    public function index() {
        $orders = $this->orderService->getData();
        $products = $this->productService->getData();

        return view('home', ['orders' => $orders, 'products' => $products]);
    }

    /**
     * @param InitService $initService
     * @param DataService $dataService
     *
     * @return bool
     * @throws ValidationException
     */
    public function addData(
        InitService $initService,
        DataService $dataService
    ) {
        $result = new JsonResponse();

        $data = $dataService->getData($initService);

        if (is_array(json_decode($data, true))
            && array_key_exists('error', json_decode($data, true))) {
            $initService->init();
            $data = $dataService->getData($initService);
        }

        $parseData = $dataService->parseData($data);

        switch ($dataService->typeData($data)) {
            case self::ORDER: $result = $this->orderService->saveData($parseData); break;
            case self::PRODUCT: $result = $this->productService->saveData($parseData); break;
        }

        session()->flash('message', json_encode($result->getData(true)));

        return true;
    }
}
