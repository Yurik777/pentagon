To install, you need to run the following commands

1. composer install
2. npm install
3. npm run dev
4. Rename .env.example to .env
5. Configure a connection to your database in a file .env
6. In .env add APP_URL=http://localhost:8000
6. php artisan migrate
7. php artisan serve
