require('./bootstrap');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    $('.message').fadeOut(5000);
    $('.btn').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/create',
            success: function (data) {
                if (data) {
                    location.reload();
                }
            }
        });
    });
})
