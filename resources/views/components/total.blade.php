<div class="total">
    <div class="total-header">{{ $header }}</div>
    <div class="total-body">
        <table class="table table-hover">
            <tr>
                <td class="table-td">ORDERS</td>
                <td class="table-td">{{ count($orders) }}</td>
            </tr>
            <tr>
                <td class="table-td">PRODUCTS</td>
                <td class="table-td">{{ count($products) }}</td>
            </tr>
        </table>
    </div>
</div>
