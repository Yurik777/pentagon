<div class="container">
    @if(session()->has('message'))
        @switch (array_keys(json_decode(session()->get('message'), true))[0])
            @case('error')
                @php $class = 'danger' @endphp
                @break
            @case('success')
                @php $class = 'success' @endphp
                @break
            @default
                @php $class = 'info' @endphp
        @endswitch
        <div class="row message">
            <div class='alert alert-{{ $class }} col-sm-12'>
                @foreach(json_decode(session()->get('message'), true) as $key => $value)
                    {{ strtoupper($key) . ': ' . $value }}
                @endforeach
                @php
                    session()->forget('message');
                @endphp
            </div>
        </div>
    @endif
</div>
