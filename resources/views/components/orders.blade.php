<div>
    <div class="header">{{ $header }}</div>
    <table class="table table-striped table-hover">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Total</th>
            <th scope="col">Shipping total</th>
            <th scope="col">Create time</th>
            <th scope="col">Timezone</th>
        </tr>
        @if(!empty($orders))
            @foreach (json_decode($orders) as $item)
                <tr>
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->total }}</td>
                    <td>{{ $item->shipping_total }}</td>
                    <td>{{ date('Y-m-d H:i', $item->create_time) }}</td>
                    <td>{{ $item->timezone }}</td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
