<div>
    <div class="header">{{ $header }}</div>
    <table class="table table-striped table-hover">
        <tr>
            <th scope="col">SKU</th>
            <th scope="col">Title</th>
            <th scope="col">Image</th>
        </tr>
        @if(!empty($products))
            @foreach (json_decode($products) as $item)
                <tr>
                    <th scope="row">{{ $item->sku }}</th>
                    <td>{{ $item->title }}</td>
                    <td><img src="data:{{ $item->image }}" class="product-image" alt="image"></td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
