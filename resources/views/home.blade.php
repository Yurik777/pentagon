@extends('app')

@section('message')
    <x-message/>
@endsection

@section('content')
<div class="container page-home">
    <div class="row justify-content-md-center">
        <div class="col-sm-9">

            <x-orders header="ORDERS" :orders="$orders"/>

            <x-products header="PRODUCTS" :products="$products"/>

        </div>
        <div class="col-sm-2">

            <x-button/>

            <x-total header="TOTAL" :orders="$orders" :products="$products"/>

        </div>
    </div>
</div>
@endsection
