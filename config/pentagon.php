<?php

return [
    'auth' => [
        'client_id' => env('API_CLIENT_ID', 'devtask'),
        'client_secret' => env('API_CLIENT_SECRET', 'Ye97T%c!CGZ*7$52')
    ],
    'api' => [
        'domain' => env('API_DOMAIN', 'https://apptest.wearepentagon.com'),
        'init' => env( 'API_INIT', '/devInterview/API/en/access-token'),
        'get_random_test_feed' => env(
            'API_GET_RANDOM_TEST_FEED',
            '/devInterview/API/en/get-random-test-feed'
        )
    ]
];
